# Detecting platform (win or unix)
VENV = if [ -d ".venv/Scripts" ]; then \
			. .venv/Scripts/activate; \
		else \
			. .venv/bin/activate; \
		fi
PIP := ${VENV} && pip
POETRY := ${HOME}/.poetry/bin/poetry
PYTEST := ${VENV} && pytest
BLACK := ${VENV} && black
COVERAGE := ${VENV} && coverage
MYPY := ${VENV} && mypy
PORT ?= 3333

venv:
	if [ ! -d ".venv" ]; then \
		python -m venv .venv; \
	fi;
	curl -sSL \
	https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py \
	| python; \

install: venv
	${POETRY} install

build: install
	${POETRY} build

test:
	${PYTEST} .

mypy:
	${MYPY} . --config .mypy.ini

test_coverage: mypy
	${COVERAGE} run -m pytest && coverage report

runserver:
	${POETRY} run server --port ${PORT}

black:
	${BLACK} .

export_requirements:
	${POETRY} lock
	${POETRY} export -f requirements.txt > requirements.txt

deploy_manual: export_requirements
	-git add .
	-git commit -m "Heroku deploy $$(date +%s)"
	-git push heroku master

clean_pyc:
	-find -iname "*.pyc" -delete

clean_cache:
	-find -type d -name "__pycache__" -exec rm -rf {} +

clean_venv:
	-rm -rf .venv

clean_package:
	-find -type d -name "*.egg-info" -exec rm -rf {} +

clean_dist:
	-rm -rf dist

clean_test:
	-rm -rf .pytest_cache
	-rm -rf coverage_html_report
	-rm .coverage
	-rm -rf .mypy_cache

clean: clean_venv clean_package clean_pyc clean_cache clean_dist clean_test
