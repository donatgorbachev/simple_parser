import pytest
from aiohttp import web

from simple_parser.server.routes import routes


@pytest.fixture
def client(loop, aiohttp_client):
    app = web.Application()
    app.router.add_routes(routes)
    return loop.run_until_complete(aiohttp_client(app))


async def test_server_ast_ok(client):
    excepted = {
        "alias": "add",
        "data": [
            10,
            {
                "alias": "div",
                "data": [
                    {"alias": "mul", "data": [5, 6]},
                    {"alias": "NAME", "data": "a"},
                ],
            },
        ],
    }
    response = await client.post("/ast", json={"expression": "10 + 5 * 6 / a"})
    json_data = await response.json()
    assert response.status == 200
    assert json_data.get("result") == excepted


async def test_server_ast_bad_data(client):
    response = await client.post("/ast", json={})
    json_data = await response.json()
    assert response.status == 400
    assert json_data.get("message") == "Send 'expression' to get AST"


async def test_server_ast_parser_error(client):
    response = await client.post("/ast", json={"expression": "14 + a; u ? 2"})
    json_data = await response.json()
    assert response.status == 400


async def test_server_calc_ok(client):
    response = await client.post(
        "/calc", json={"expression": "b = 5 * a; 10 / b", "context": {"a": 2}}
    )
    json_data = await response.json()
    assert response.status == 200
    assert json_data.get("result") == 1


async def test_server_calc_bad_data(client):
    response = await client.post("/calc", json={})
    json_data = await response.json()
    assert response.status == 400
    assert json_data.get("message") == "Send 'expression' and 'context' to get result"


async def test_server_calc_parser_error(client):
    response = await client.post("/calc", json={"expression": "b = 14 + a; 2 + b"})
    json_data = await response.json()
    assert response.status == 400
