import json
import pytest
from lark import Tree

from simple_parser import ArithmeticParser, ArithmeticParserError


@pytest.mark.parametrize(
    "expression,expected",
    [
        ("5 + 13", Tree("add", [5, 13])),
        ("20 - 1.5", Tree("sub", [20, 1.5])),
        ("33 * 75", Tree("mul", [33, 75])),
        ("1 / 102", Tree("div", [1, 102])),
        ("-2", Tree("neg", [2])),
        ("10 + 5 * 6", Tree("add", [10, Tree("mul", [5, 6])])),
        (
            "(20 - 4) / 102 + 60 * (4 + 3)",
            Tree(
                "add",
                [
                    Tree("div", [Tree("sub", [20, 4]), 102]),
                    Tree("mul", [60, Tree("add", [4, 3]),]),
                ],
            ),
        ),
        ("10 + a", Tree("add", [10, Tree("NAME", "a")])),
        (
            "a + b + c",
            Tree(
                "add",
                [
                    Tree("add", [Tree("NAME", "a"), Tree("NAME", "b")]),
                    Tree("NAME", "c"),
                ],
            ),
        ),
        (
            "a + (-b)",
            Tree("add", [Tree("NAME", "a"), Tree("neg", [Tree("NAME", "b")])]),
        ),
        ("a = 1; 2 + a", Tree("add", [2, 1])),
        ("b = 5 * a; 10 / b", Tree("div", [10, Tree("mul", [5, Tree("NAME", "a")])])),
    ],
)
def test_arithmetic_parser_expression(expression: str, expected: Tree):
    parser = ArithmeticParser()
    assert parser.parse(expression) == expected


@pytest.mark.parametrize("expression", ["", "a==1", "7 ? a", "9f2 + 2", "a = 2; u & a"])
def test_arithmetic_parser_error(expression: str):
    parser = ArithmeticParser()
    with pytest.raises(ArithmeticParserError):
        parser.parse(expression)


@pytest.mark.parametrize(
    "expression,expected",
    [
        ("5 + 13", {"alias": "add", "data": [5, 13]}),
        ("-2", {"alias": "neg", "data": 2}),
        (
            "10 + 5 * 6",
            {"alias": "add", "data": [10, {"alias": "mul", "data": [5, 6]}]},
        ),
        ("10 + a", {"alias": "add", "data": [10, {"alias": "NAME", "data": "a"}]}),
    ],
)
def test_arithmetic_parser_parse_to_dict(expression: str, expected: dict):
    parser = ArithmeticParser()
    assert parser.parse_to_dict(expression) == expected


@pytest.mark.parametrize(
    "expression,context,expected",
    [
        ("5 + 13", {}, 18),
        ("20 - 1.5", {}, 18.5),
        ("33 * 75", {}, 2475),
        ("1 / 102", {}, 1 / 102),
        ("-2", {}, -2),
        ("10 + 5 * 6", {}, 40),
        ("(20 - 4) / 102 + 60 * (4 + 3)", {}, (20 - 4) / 102 + 60 * (4 + 3)),
        ("10 + a", {"a": 5}, 15),
        ("a + b + c", {"a": -4, "b": 4, "c": -1}, -1),
        ("a = 1; 2 + a", {}, 3),
        ("b = 5 * a; 10 / b", {"a": 2}, 1),
    ],
)
def test_arithmetic_parser_calculate_expression(
    expression: str, context: dict, expected: Tree
):
    parser = ArithmeticParser()
    assert parser.calculate(expression, context) == expected


@pytest.mark.parametrize(
    "expression,context",
    [("a + 13", {}), ("a = 1; a + b + c", {"b": 5}), ("a ? 5", {"a": 1}), ("", {})],
)
def test_arithmetic_parser_calculate_expression_error(expression: str, context: dict):
    parser = ArithmeticParser()
    with pytest.raises(ArithmeticParserError):
        parser.calculate(expression, context)
