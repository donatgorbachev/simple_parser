from aiohttp import web

from .routes import routes


app = web.Application()
app.router.add_routes(routes)
