from aiohttp import web

from simple_parser.parsers import ArithmeticParser, ArithmeticParserError


routes = web.RouteTableDef()


@routes.post("/ast")
async def handle_ast(request):
    post_data = await request.json()
    expression = post_data.get("expression")

    if not expression:
        return web.json_response(
            {"message": "Send 'expression' to get AST",}, status=400
        )

    try:
        data = ArithmeticParser().parse_to_dict(expression)
        return web.json_response({"result": data})
    except ArithmeticParserError as e:
        return web.json_response({"message": str(e)}, status=400)


@routes.post("/calc")
async def handle_calc(request):
    post_data = await request.json()
    expression = post_data.get("expression")
    context = post_data.get("context", {})

    if not expression:
        return web.json_response(
            {"message": "Send 'expression' and 'context' to get result",}, status=400,
        )

    try:
        data = ArithmeticParser().calculate(expression, context)
        return web.json_response({"result": data})
    except ArithmeticParserError as e:
        return web.json_response({"message": str(e)}, status=400)
