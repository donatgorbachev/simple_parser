from typing import List, Union, Dict
from lark import Lark, Transformer, Tree, v_args, UnexpectedInput


@v_args(inline=True)
class ArithmeticTree(Transformer):
    number = float

    def __init__(self) -> None:
        self.vars: Dict[str, float] = {}

    def assign_var(self, name, value) -> float:
        self.vars[name] = value
        return value

    def var(self, name) -> Union[float, Tree]:
        return self.vars.get(name, Tree("NAME", name))


@v_args(inline=True)
class ArithmeticCalculateTree(Transformer):
    from operator import add, sub, mul, truediv as div, neg

    number = float

    def assign_var(self, name, value) -> float:
        self.vars[name] = value
        return value

    def var(self, name) -> Union[float, Tree, None]:
        return self.vars.get(name)

    def __init__(self, context: dict) -> None:
        self.vars = context.copy()


def tree_to_dict(node: Union[Tree, float]) -> Union[Tree, float]:
    if not isinstance(node, Tree):
        return node
    if isinstance(node.children, list):
        data = (
            [tree_to_dict(child) for child in node.children]
            if len(node.children) > 1
            else tree_to_dict(node.children[0])
        )
    else:
        data = tree_to_dict(node.children)
    return {"alias": node.data, "data": data}


class ArithmeticParserError(Exception):
    pass


class ArithmeticParser:
    grammer = """
        ?start: sum
            | NAME "=" sum      -> assign_var
        ?sum: product
            | sum "+" product   -> add
            | sum "-" product   -> sub
        ?product: atom
            | product "*" atom  -> mul
            | product "/" atom  -> div
        ?atom: NUMBER           -> number
            | "-" atom          -> neg
            | NAME              -> var
            | "(" sum ")"
        %import common.CNAME    -> NAME
        %import common.NUMBER
        %import common.WS_INLINE
        %ignore WS_INLINE
    """

    @staticmethod
    def prepare_input(input_string: str) -> List[str]:
        return input_string.replace(" ", "").split(";")

    @staticmethod
    def parse_lines(input_lines: List[str], parser: Lark) -> Tree:
        result = None
        for line in input_lines:
            result = parser.parse(line)
        return result

    def parse(self, input_string: str) -> Tree:
        input_lines = self.prepare_input(input_string)

        parser = Lark(self.grammer, parser="lalr", transformer=ArithmeticTree())

        try:
            result = self.parse_lines(input_lines, parser)
        except UnexpectedInput as e:
            raise ArithmeticParserError(str(e))

        return result

    def parse_to_dict(self, input_string: str) -> Union[dict, float]:
        return tree_to_dict(self.parse(input_string))

    def calculate(self, input_string: str, context: dict) -> float:
        input_lines = self.prepare_input(input_string)
        parser = Lark(
            self.grammer, parser="lalr", transformer=ArithmeticCalculateTree(context)
        )

        try:
            result = self.parse_lines(input_lines, parser)
        except UnexpectedInput as e:
            raise ArithmeticParserError(str(e))
        except TypeError:
            raise ArithmeticParserError("Not all variables in context")

        return result
