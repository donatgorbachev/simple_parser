import click
from aiohttp import web

from simple_parser.server import app


@click.group()
def cli():
    pass


@cli.command()
@click.option("--port", type=int, default=80)
def runserver(port):
    """
    Starts server
    """
    web.run_app(app, port=port)


if __name__ == "__main__":
    cli()
